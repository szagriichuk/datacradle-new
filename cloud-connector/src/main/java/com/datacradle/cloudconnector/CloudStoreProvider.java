package com.datacradle.cloudconnector;

import com.datacradle.cloudconnector.authenticate.AuthenticationParameters;
import com.datacradle.cloudconnector.blobstore.CloudASyncBlobStore;
import com.datacradle.cloudconnector.blobstore.CloudBlobStore;
import com.datacradle.cloudconnector.blobstore.CloudSyncBlobStore;
import com.datacradle.cloudconnector.clouds.Cloud;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStoreContext;

/**
 * @author sergiizagriichuk
 */
public class CloudStoreProvider {

    private Cloud cloud;
    private AuthenticationParameters authenticationParameters;
    private String containerName;

    private CloudStoreProvider() {
    }

    public static CloudStoreProvider createCloudStoreProvider() {
        return new CloudStoreProvider();
    }

    public CloudStoreProvider forCloud(Cloud cloud) {
        this.cloud = cloud;
        return this;
    }

    public CloudStoreProvider usingAuthenticationParameters(AuthenticationParameters parameters) {
        this.authenticationParameters = parameters;
        return this;
    }

    public CloudStoreProvider usingCloudContainer(String cloudContainer) {
        this.containerName = cloudContainer;
        return this;
    }

    public CloudBlobStore createSyncCloudStore() {

        checkParameters();

        CloudSyncBlobStore cloudSyncBlobStore = getSyncCloudStore(cloud, authenticationParameters);

        cloudSyncBlobStore.createContainer(containerName);

        clearParameters();

        return cloudSyncBlobStore;
    }

    public CloudBlobStore createASyncCloudStore() {

        checkParameters();

        CloudASyncBlobStore cloudASyncBlobStore = getASyncCloudStore(cloud, authenticationParameters);

        cloudASyncBlobStore.createContainer(containerName);

        clearParameters();

        return cloudASyncBlobStore;
    }

    private void checkParameters() {
        if (cloud == null || authenticationParameters == null || containerName == null)
            throw new CloudStoreProviderException("You have to set up cloud, authenticationParameters and containerName before using.");
    }

    private void clearParameters() {
        cloud = null;
        authenticationParameters = null;
        containerName = null;
    }

    private CloudSyncBlobStore getSyncCloudStore(Cloud cloud, AuthenticationParameters parameters) {

        BlobStoreContext blobStoreContext = buildBlobStoreContext(cloud, parameters);

        return new CloudSyncBlobStore(blobStoreContext.getBlobStore());
    }

    private CloudASyncBlobStore getASyncCloudStore(Cloud cloud, AuthenticationParameters parameters) {

        BlobStoreContext blobStoreContext = buildBlobStoreContext(cloud, parameters);

        return new CloudASyncBlobStore(blobStoreContext.getAsyncBlobStore());
    }


    private BlobStoreContext buildBlobStoreContext(Cloud cloud, AuthenticationParameters parameters) {
        ContextBuilder contextBuilder = createContextBuilder(cloud, parameters);

        return contextBuilder.buildView(BlobStoreContext.class);
    }

    private ContextBuilder createContextBuilder(Cloud cloud, AuthenticationParameters parameters) {

        ContextBuilder contextBuilder = ContextBuilder.newBuilder(cloud.getCloudName());

        contextBuilder.credentials(parameters.getKey(), parameters.getPass());

        if (parameters.getOverWrittenProperties() != null)
            contextBuilder.overrides(parameters.getOverWrittenProperties());

        return contextBuilder;
    }
}
