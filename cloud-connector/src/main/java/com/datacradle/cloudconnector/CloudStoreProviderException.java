package com.datacradle.cloudconnector;

/**
 * @author sergiizagriichuk
 */
public class CloudStoreProviderException extends RuntimeException {
    public CloudStoreProviderException(String message) {
        super(message);
    }
}
