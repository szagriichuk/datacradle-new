package com.datacradle.cloudconnector.authenticate;

import java.util.Properties;

/**
 * @author sergiizagriichuk
 */
public class AuthenticationParameters {

    private String key;
    private String pass;
    private Properties overWrittenProperties;

    public AuthenticationParameters(String key, String pass) {
        this.key = key;
        this.pass = pass;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Properties getOverWrittenProperties() {
        return overWrittenProperties;
    }

    public void setOverWrittenProperties(Properties overWrittenProperties) {
        this.overWrittenProperties = overWrittenProperties;
    }
}
