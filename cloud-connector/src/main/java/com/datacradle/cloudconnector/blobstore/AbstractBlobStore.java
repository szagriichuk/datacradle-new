package com.datacradle.cloudconnector.blobstore;

import com.datacradle.cloudconnector.logger.LoggerFacade;
import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * @author sergiizagriichuk
 */
public abstract class AbstractBlobStore implements CloudBlobStore {

    String containerName;

    protected static Logger logger = LoggerFacade.getLogger(AbstractBlobStore.class);

    protected byte[] readDataFromStream(InputStream stream) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        if (stream != null) {
            out = readDataToBuffer(stream);
        }
        return out.toByteArray();
    }

    private ByteArrayOutputStream readDataToBuffer(InputStream stream) {
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            while (stream.read(buffer) != -1)
                byteArrayOutputStream.write(buffer);
        } catch (Exception e) {
            logger.warn(e.getMessage());
        }

        return byteArrayOutputStream;
    }
}
