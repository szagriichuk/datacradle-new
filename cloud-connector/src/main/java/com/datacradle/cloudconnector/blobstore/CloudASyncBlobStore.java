package com.datacradle.cloudconnector.blobstore;

import com.datacradle.cloudconnector.blobstore.blobvalue.BlobValue;
import org.jclouds.blobstore.AsyncBlobStore;

import java.util.concurrent.ExecutionException;

/**
 * @author sergiizagriichuk
 */
public class CloudASyncBlobStore extends AbstractBlobStore {
    private AsyncBlobStore blobStore;

    public CloudASyncBlobStore(AsyncBlobStore blobStore) {
        this.blobStore = blobStore;
    }

    @Override
    public void createContainer(String containerName) {
        this.containerName = containerName;

        try {

            if (!blobStore.containerExists(containerName).get())
                blobStore.createContainerInLocation(null, containerName);

        } catch (InterruptedException | ExecutionException e) {
            logger.error(e.getMessage());
        }
    }

    public String saveData(String key, BlobValue value) {
        //To change body of implemented methods use File | Settings | File Templates.
        return null;
    }

    public BlobValue getData(String key) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void removeData(String key) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
