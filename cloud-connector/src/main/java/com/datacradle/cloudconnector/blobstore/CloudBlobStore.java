package com.datacradle.cloudconnector.blobstore;

import com.datacradle.cloudconnector.blobstore.blobvalue.BlobValue;

/**
 * @author sergiizagriichuk
 */
public interface CloudBlobStore {
    void createContainer(String containerName);

    String saveData(String key, BlobValue value);

    BlobValue getData(String key);

    void removeData(String key);
}
