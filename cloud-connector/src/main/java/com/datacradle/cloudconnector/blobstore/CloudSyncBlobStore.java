package com.datacradle.cloudconnector.blobstore;

import com.datacradle.cloudconnector.blobstore.blobvalue.BlobValue;
import com.datacradle.cloudconnector.blobstore.blobvalue.factories.BlobValueFactory;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.io.Payload;

/**
 * @author sergiizagriichuk
 */
public class CloudSyncBlobStore extends AbstractBlobStore {
    private BlobStore blobStore;

    public CloudSyncBlobStore(BlobStore blobStore) {
        this.blobStore = blobStore;
    }

    @Override
    public void createContainer(String containerName) {
        this.containerName = containerName;

        if (!blobStore.containerExists(containerName))
            blobStore.createContainerInLocation(null, containerName);

    }

    public String saveData(String key, BlobValue value) {
        return blobStore.putBlob(containerName, buildBlob(key, value));
    }

    private Blob buildBlob(String key, BlobValue value) {
        return blobStore.blobBuilder(key).payload((Payload) value.getPayload()).build();
    }

    public BlobValue getData(String key) {
        Payload payload = blobStore.getBlob(containerName, key).getPayload();
        return BlobValueFactory.createBlobValueFromData(readDataFromStream(payload.getInput()));
    }

    public void removeData(String key) {
        blobStore.removeBlob(containerName, key);
    }
}
