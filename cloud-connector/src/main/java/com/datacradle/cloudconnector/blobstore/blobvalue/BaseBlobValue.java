package com.datacradle.cloudconnector.blobstore.blobvalue;

import org.jclouds.io.Payload;
import org.jclouds.io.payloads.BasePayload;

import java.io.InputStream;

/**
 * @author sergiizagriichuk
 */
public abstract class BaseBlobValue<T> implements BlobValue<T, Payload> {

    public BaseBlobValue(BasePayload<T> payload) {
        this.payload = payload;
    }

    BasePayload<T> payload;

    public Payload getPayload() {
        return payload;
    }

    public T getRawContent() {
        return payload.getRawContent();
    }

    public InputStream getInputStream() {
        return payload.getInput();
    }
}
