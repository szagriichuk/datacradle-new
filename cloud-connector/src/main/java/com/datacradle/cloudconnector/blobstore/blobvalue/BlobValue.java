package com.datacradle.cloudconnector.blobstore.blobvalue;


import java.io.InputStream;

/**
 * @author sergiizagriichuk
 */
public interface BlobValue<T, V> {
    V getPayload();

    T getRawContent();

    InputStream getInputStream();
}
