package com.datacradle.cloudconnector.blobstore.blobvalue;


import org.jclouds.io.payloads.ByteArrayPayload;

/**
 * @author sergiizagriichuk
 */
public class ByteArrayBlobValue extends BaseBlobValue<byte[]> {

    public ByteArrayBlobValue(byte[] data) {
        this(new ByteArrayPayload(data));
    }

    public ByteArrayBlobValue(ByteArrayPayload payload) {
        super(payload);
    }
}
