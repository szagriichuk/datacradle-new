package com.datacradle.cloudconnector.blobstore.blobvalue;

import org.jclouds.io.payloads.FilePayload;

import java.io.File;

/**
 * @author sergiizagriichuk
 */
public class FileBlobValue extends BaseBlobValue<File> {

    public FileBlobValue(File data) {
        this(new FilePayload(data));
    }

    public FileBlobValue(FilePayload payload) {
        super(payload);
    }
}
