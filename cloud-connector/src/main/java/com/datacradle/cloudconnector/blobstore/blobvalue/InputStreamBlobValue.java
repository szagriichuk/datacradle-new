package com.datacradle.cloudconnector.blobstore.blobvalue;

import org.jclouds.io.payloads.InputStreamPayload;

import java.io.InputStream;

/**
 * @author sergiizagriichuk
 */
public class InputStreamBlobValue extends BaseBlobValue<InputStream> {

    public InputStreamBlobValue(InputStream data) {
        this(new InputStreamPayload(data));
    }

    public InputStreamBlobValue(InputStreamPayload payload) {
        super(payload);
    }
}
