package com.datacradle.cloudconnector.blobstore.blobvalue;

import org.jclouds.io.payloads.StringPayload;

/**
 * @author sergiizagriichuk
 */
public class StringBlobValue extends BaseBlobValue<String> {

    public StringBlobValue(String data) {
        this(new StringPayload(data));
    }

    public StringBlobValue(StringPayload payload) {
        super(payload);
    }
}
