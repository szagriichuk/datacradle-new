package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

import com.datacradle.cloudconnector.blobstore.blobvalue.BaseBlobValue;
import org.jclouds.io.Payload;
import org.jclouds.io.payloads.ByteArrayPayload;
import org.jclouds.io.payloads.FilePayload;
import org.jclouds.io.payloads.InputStreamPayload;
import org.jclouds.io.payloads.StringPayload;

import java.io.File;
import java.io.InputStream;

/**
 * @author sergiizagriichuk
 */
public class BlobValueFactory {

    public static <T> BaseBlobValue<T> createBlobValue(T data) {

        if (data instanceof Payload) {

            return createBlobValueFromPayload((Payload) data);

        } else {

            return createBlobValueFromData(data);
        }
    }

    public static <T> BaseBlobValue<T> createBlobValueFromData(T data) {

        if (data instanceof byte[]) {

            return (BaseBlobValue<T>) ByteArrayBlobValueFactory.createByteArrayBlobValue((byte[]) data);

        } else if (data instanceof String) {

            return (BaseBlobValue<T>) StringBlobValueFactory.createStringBlobValue((String) data);

        } else if (data instanceof File) {

            return (BaseBlobValue<T>) FileBlobValueFactory.createFileBlobValue((File) data);

        } else if (data instanceof InputStream) {

            return (BaseBlobValue<T>) InputStreamBlobValueFactory.createInputStreamBlobValue((InputStream) data);
        }

        throw new UnsupportedBlobValueTypeException(data);
    }

    public static BaseBlobValue createBlobValueFromPayload(Payload data) {

        if (data instanceof ByteArrayPayload) {

            return ByteArrayBlobValueFactory.createByteArrayBlobValue((ByteArrayPayload) data);

        } else if (data instanceof StringPayload) {

            return StringBlobValueFactory.createStringBlobValue((StringPayload) data);

        } else if (data instanceof FilePayload) {

            return FileBlobValueFactory.createFileBlobValue((FilePayload) data);

        } else if (data instanceof InputStreamPayload) {

            return InputStreamBlobValueFactory.createInputStreamBlobValue((InputStreamPayload) data);

        }

        throw new UnsupportedBlobValueTypeException(data);
    }
}
