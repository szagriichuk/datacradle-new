package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

import com.datacradle.cloudconnector.blobstore.blobvalue.BaseBlobValue;
import com.datacradle.cloudconnector.blobstore.blobvalue.ByteArrayBlobValue;
import org.jclouds.io.payloads.ByteArrayPayload;

public class ByteArrayBlobValueFactory {
    public static BaseBlobValue<byte[]> createByteArrayBlobValue(byte[] data) {
        return new ByteArrayBlobValue(data);
    }

    public static BaseBlobValue<byte[]> createByteArrayBlobValue(ByteArrayPayload data) {
        return new ByteArrayBlobValue(data);
    }
}