package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

import com.datacradle.cloudconnector.blobstore.blobvalue.BaseBlobValue;
import com.datacradle.cloudconnector.blobstore.blobvalue.FileBlobValue;
import org.jclouds.io.payloads.FilePayload;

import java.io.File;

/**
 * @author sergiizagriichuk
 */
public class FileBlobValueFactory {

    public static BaseBlobValue<File> createFileBlobValue(File data) {
        return new FileBlobValue(data);
    }

    public static BaseBlobValue<File> createFileBlobValue(FilePayload data) {
        return new FileBlobValue(data);
    }
}
