package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

import com.datacradle.cloudconnector.blobstore.blobvalue.BaseBlobValue;
import com.datacradle.cloudconnector.blobstore.blobvalue.InputStreamBlobValue;
import org.jclouds.io.payloads.InputStreamPayload;

import java.io.InputStream;

/**
 * @author sergiizagriichuk
 */
public class InputStreamBlobValueFactory {

    public static BaseBlobValue<InputStream> createInputStreamBlobValue(InputStream data) {
        return new InputStreamBlobValue(data);
    }

    public static BaseBlobValue<InputStream> createInputStreamBlobValue(InputStreamPayload data) {
        return new InputStreamBlobValue(data);
    }
}
