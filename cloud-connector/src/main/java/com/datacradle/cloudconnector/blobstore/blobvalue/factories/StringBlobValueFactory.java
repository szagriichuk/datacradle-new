package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

import com.datacradle.cloudconnector.blobstore.blobvalue.BaseBlobValue;
import com.datacradle.cloudconnector.blobstore.blobvalue.StringBlobValue;
import org.jclouds.io.payloads.StringPayload;

public class StringBlobValueFactory {
    public static BaseBlobValue<String> createStringBlobValue(String data) {
        return new StringBlobValue(data);
    }

    public static BaseBlobValue<String> createStringBlobValue(StringPayload data) {
        return new StringBlobValue(data);
    }
}