package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

/**
 * @author sergiizagriichuk
 */
public class UnsupportedBlobValueTypeException extends UnsupportedOperationException {

    public UnsupportedBlobValueTypeException(Object type) {
        super("This type " + type + " is unsupported by current version of factory.");
    }

}
