package com.datacradle.cloudconnector.clouds;

/**
 * @author sergiizagriichuk
 */
public interface Cloud {

    public String getCloudName();
}
