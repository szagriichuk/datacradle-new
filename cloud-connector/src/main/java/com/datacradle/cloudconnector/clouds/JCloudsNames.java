package com.datacradle.cloudconnector.clouds;

/**
 * @author sergiizagriichuk
 */
public enum JCloudsNames implements Cloud {
    AMAZON("aws-s3"), AZURE("azureblob");

    private JCloudsNames(String name) {
        this.name = name;
    }

    private String name;

    public String getCloudName() {
        return name;
    }
}
