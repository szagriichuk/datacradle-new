package com.datacradle.cloudconnector;

import com.datacradle.cloudconnector.authenticate.AuthenticationParameters;
import com.datacradle.cloudconnector.blobstore.CloudBlobStore;
import com.datacradle.cloudconnector.blobstore.blobvalue.BlobValue;
import com.datacradle.cloudconnector.blobstore.blobvalue.ByteArrayBlobValue;
import com.datacradle.cloudconnector.blobstore.blobvalue.StringBlobValue;
import com.datacradle.cloudconnector.blobstore.blobvalue.factories.BlobValueFactory;
import com.datacradle.cloudconnector.clouds.JCloudsNames;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author sergiizagriichuk
 */
public class CloudBlobStoreRealAccountTest {

    @Test
    public void testSaveDataToAmazon() throws Exception {
        CloudBlobStore cloudBlobStore = getCloudBlobStore();

        String value = "Test";

        cloudBlobStore.saveData("key1", BlobValueFactory.createBlobValue(value));

        Assert.assertEquals(value, getStringValue(cloudBlobStore.getData("key1")));

    }

    private String getStringValue(BlobValue blobValue) {
        String result = "";
        if (blobValue instanceof ByteArrayBlobValue)
            result = new String((byte[]) blobValue.getRawContent()).trim();
        else if (blobValue instanceof StringBlobValue)
            result = (String) blobValue.getRawContent();

        return result;
    }

    private CloudBlobStore getCloudBlobStore() {
        return CloudStoreProvider.createCloudStoreProvider().
                forCloud(JCloudsNames.AMAZON).
                usingAuthenticationParameters(new AuthenticationParameters("AKIAIPJFI7MPQCEMQESQ", "xd++84OWmhG/bjl2j87CCIi+yZ2f16xMG0kO/V/D")).
                usingCloudContainer("testContainer").createSyncCloudStore();
    }
}
