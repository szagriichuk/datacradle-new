package com.datacradle.cloudconnector.authenticate;

import junit.framework.Assert;
import org.junit.Test;

import java.util.Properties;

/**
 * @author sergiizagriichuk
 */
public class AuthenticationParametersTest {
    @Test
    public void testKey() throws Exception {
        AuthenticationParameters authenticationParameters = new AuthenticationParameters("key1", null);

        Assert.assertEquals("key1", authenticationParameters.getKey());
    }

    @Test
    public void testNullKey() throws Exception {
        AuthenticationParameters authenticationParameters = new AuthenticationParameters(null, null);

        Assert.assertEquals(null, authenticationParameters.getKey());
    }

    @Test
    public void testChangingKey() throws Exception {
        AuthenticationParameters authenticationParameters = new AuthenticationParameters("key1", null);

        Assert.assertEquals("key1", authenticationParameters.getKey());

        authenticationParameters.setKey("key2");

        Assert.assertEquals("key2", authenticationParameters.getKey());

    }

    @Test
    public void testPass() throws Exception {
        AuthenticationParameters authenticationParameters = new AuthenticationParameters(null, "pass");

        Assert.assertEquals("pass", authenticationParameters.getPass());
    }

    @Test
    public void testNullPass() throws Exception {
        AuthenticationParameters authenticationParameters = new AuthenticationParameters(null, null);

        Assert.assertEquals(null, authenticationParameters.getPass());
    }

    @Test
    public void testChangingPass() throws Exception {
        AuthenticationParameters authenticationParameters = new AuthenticationParameters(null, "pass");

        Assert.assertEquals("pass", authenticationParameters.getPass());

        authenticationParameters.setPass("pass1");

        Assert.assertEquals("pass1", authenticationParameters.getPass());
    }

    @Test
    public void testProperties() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("key1", "val");

        AuthenticationParameters authenticationParameters = new AuthenticationParameters(null, null);

        authenticationParameters.setOverWrittenProperties(properties);

        Assert.assertNotNull(authenticationParameters.getOverWrittenProperties());

        Assert.assertEquals(properties.get("key1"), authenticationParameters.getOverWrittenProperties().get("key1"));
    }
}
