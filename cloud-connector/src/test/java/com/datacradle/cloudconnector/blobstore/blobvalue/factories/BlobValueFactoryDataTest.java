package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

import com.datacradle.cloudconnector.blobstore.blobvalue.*;
import org.jclouds.io.Payload;
import org.jclouds.io.payloads.ByteArrayPayload;
import org.jclouds.io.payloads.FilePayload;
import org.jclouds.io.payloads.InputStreamPayload;
import org.jclouds.io.payloads.StringPayload;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @author sergiizagriichuk
 */
public class BlobValueFactoryDataTest extends GenericBlobValueFactoryTest {

    @Test
    public void testCreateByteArrayBlobValue() throws Exception {

        byte[] data = new byte[]{1, 2, 3, 4};

        BlobValue<? extends byte[], ? extends Payload> blobValue = BlobValueFactory.createBlobValue(data);

        testBlobValue(data, blobValue, ByteArrayBlobValue.class, ByteArrayPayload.class);
    }

    @Test
    public void testCreateStringBlobValue() throws Exception {

        String data = "tro-lo-lo";

        BlobValue<? extends String, ? extends Payload> blobValue = BlobValueFactory.createBlobValue(data);

        testBlobValue(data, blobValue, StringBlobValue.class, StringPayload.class);
    }

    @Test
    public void testCreateFileBlobValue() throws Exception {

        File data = new File("./src/test/java/testfile.txt");

        BlobValue<? extends File, ? extends Payload> blobValue = BlobValueFactory.createBlobValue(data);

        testBlobValue(data, blobValue, FileBlobValue.class, FilePayload.class);

        Assert.assertArrayEquals(readDataFromStream(new FileInputStream(data)), readDataFromStream(blobValue.getInputStream()));
    }

    @Test
    public void testCreateInputStreamBlobValue() throws Exception {

        FileInputStream data = new FileInputStream(new File("./src/test/java/testfile.txt"));

        InputStream[] streams = cloneStream(data, 2);

        BlobValue<? extends InputStream, ? extends Payload> blobValue = BlobValueFactory.createBlobValue(streams[0]);

        testBlobValue(streams[0], blobValue, InputStreamBlobValue.class, InputStreamPayload.class);

        Assert.assertArrayEquals(readDataFromStream(streams[1]), readDataFromStream(blobValue.getInputStream()));
    }

    @Test(expected = UnsupportedBlobValueTypeException.class)
    public void testCreateNullBlobValue() throws Exception {

        BlobValue blobValue = BlobValueFactory.createBlobValue(null);

        Assert.assertNotNull(blobValue);
    }
}
