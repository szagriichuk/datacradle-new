package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

import com.datacradle.cloudconnector.blobstore.blobvalue.*;
import org.jclouds.io.payloads.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @author sergiizagriichuk
 */
public class BlobValueFactoryPayloadTest extends GenericBlobValueFactoryTest {
    @Test
    public void testCreateByteArrayBlobValueFromPayload() throws Exception {

        ByteArrayPayload data = new ByteArrayPayload(new byte[]{1, 2, 3, 4});

        BlobValue blobValue = BlobValueFactory.createBlobValue(data);

        testBlobValue(data.getRawContent(), blobValue, ByteArrayBlobValue.class, ByteArrayPayload.class);
    }

    @Test
    public void testCreateStringBlobValueFromPayload() throws Exception {

        StringPayload data = new StringPayload("tro-lo-lo");

        BlobValue blobValue = BlobValueFactory.createBlobValue(data);

        testBlobValue(data.getRawContent(), blobValue, StringBlobValue.class, StringPayload.class);
    }

    @Test
    public void testCreateFileBlobValueFromPayload() throws Exception {

        FilePayload data = new FilePayload(new File("./src/test/java/testfile.txt"));

        BlobValue blobValue = BlobValueFactory.createBlobValue(data);

        testBlobValue(data.getRawContent(), blobValue, FileBlobValue.class, FilePayload.class);

        Assert.assertArrayEquals(readDataFromStream(new FileInputStream(data.getRawContent())), readDataFromStream(blobValue.getInputStream()));
    }

    @Test
    public void testCreateInputStreamBlobValueFromPayload() throws Exception {

        InputStream[] streams = cloneStream(new FileInputStream(new File("./src/test/java/testfile.txt")), 2);

        InputStreamPayload data0 = new InputStreamPayload(streams[0]);

        InputStreamPayload data1 = new InputStreamPayload(streams[1]);

        BlobValue blobValue = BlobValueFactory.createBlobValue(data0);

        testBlobValue(data0.getRawContent(), blobValue, InputStreamBlobValue.class, InputStreamPayload.class);

        Assert.assertArrayEquals(readDataFromStream(data1.getInput()), readDataFromStream(blobValue.getInputStream()));
    }

    @Test(expected = UnsupportedBlobValueTypeException.class)
    public void testUnsupportedPayload() throws Exception {

        BlobValue blobValue = BlobValueFactory.createBlobValue(new PhantomPayload());
        Assert.assertNotNull(blobValue);
    }
}
