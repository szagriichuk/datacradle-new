package com.datacradle.cloudconnector.blobstore.blobvalue.factories;

import com.datacradle.cloudconnector.blobstore.blobvalue.BlobValue;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * @author sergiizagriichuk
 */
public class GenericBlobValueFactoryTest {

    private Logger logger = LoggerFactory.getLogger(GenericBlobValueFactoryTest.class);

    void testBlobValue(Object data, BlobValue blobValue, Class<?> assertBlobValueClass, Class<?> assertPayloadClass) {

        Assert.assertEquals(assertBlobValueClass, blobValue.getClass());

        Assert.assertEquals(assertPayloadClass, blobValue.getPayload().getClass());

        Assert.assertEquals(data, blobValue.getRawContent());
    }

    InputStream[] cloneStream(FileInputStream data, int streamCount) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int len;

        while ((len = data.read(buffer)) > -1) {
            byteArrayOutputStream.write(buffer, 0, len);
        }

        byteArrayOutputStream.flush();

        return createStreams(streamCount, byteArrayOutputStream);
    }

    InputStream[] createStreams(int streamCount, ByteArrayOutputStream byteArrayOutputStream) {
        InputStream[] streams = new InputStream[streamCount];

        for (int index = 0; index < streams.length; index++) {
            streams[index] = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        }

        return streams;
    }

    byte[] readDataFromStream(InputStream inputStream) {

        byte[] bytes = new byte[0];

        try {
            bytes = new byte[inputStream.available()];
            while (inputStream.read(bytes) != -1) {
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return bytes;
    }

}
